package com.example.armando.ejandroid03;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    private EditText edT1;
    private EditText edT2;
    private TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edT1 = (EditText) findViewById(R.id.edT1);
        edT2 = (EditText) findViewById(R.id.edT2);
        tv1  = (TextView) findViewById(R.id.tv1);
    }

    public void sumar(View view)
    {
        String numero1 = edT1.getText().toString();
        String numero2 = edT2.getText().toString();

        int n1 = Integer.parseInt(numero1);
        int n2 = Integer.parseInt(numero2);

        int suma = n1 + n2;

        String resultado = String.valueOf(suma);
        tv1.setText(resultado);
    }

    public void restar(View view)
    {
        String numero1 = edT1.getText().toString();
        String numero2 = edT2.getText().toString();

        int n1 = Integer.parseInt(numero1);
        int n2 = Integer.parseInt(numero2);

        int resta = n1 - n2;

        String resultado = String.valueOf(resta);
        tv1.setText(resultado);
    }

    public void multiplicar(View view)
    {
        String numero1 = edT1.getText().toString();
        String numero2 = edT2.getText().toString();

        int n1 = Integer.parseInt(numero1);
        int n2 = Integer.parseInt(numero2);

        int multiplica = n1 * n2;

        String resultado = String.valueOf(multiplica);
        tv1.setText(resultado);
    }

    public void dividir(View view)
    {
        String numero1 = edT1.getText().toString();
        String numero2 = edT2.getText().toString();

        float n1 = Float.parseFloat(numero1);
        float n2 = Float.parseFloat(numero2);

        float divide = n1 / n2;

        String resultado = String.valueOf(divide);
        tv1.setText(resultado);
    }

}
